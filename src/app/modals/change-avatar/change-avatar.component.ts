import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ModalController, ToastController } from '@ionic/angular';
import { UserService } from 'src/app/users/services/user.service';

@Component({
  selector: 'app-change-avatar',
  templateUrl: './change-avatar.component.html',
  styleUrls: ['./change-avatar.component.scss']
})
export class ChangeAvatarComponent implements OnInit {

  imagen;
  toast;
  constructor(    private camera: Camera,public modalCtrl: ModalController, 
    public toastController: ToastController, public userServ: UserService) { }

  ngOnInit(  ) {
  }

  takePhoto() {
    const options: CameraOptions = {
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };
    this.getPicture(options);
  }

  pickFromGallery() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };
    this.getPicture(options);
  }

  private getPicture(options: CameraOptions) {
    this.camera.getPicture(options).then((imageData) => {
      // If it's base64:
      this.imagen = 'data:image/jpeg;base64,' + imageData;
    }).catch((err) => {
      // Handle error
    });
  }

  private changeAvatar(){
    if (this.imagen == "") {
      this.showToast(3000, 'Not valid imagen');
      return;
    }

    this.userServ.changeAvatarProfile(this.imagen).subscribe(
        ok => this.modalCtrl.dismiss({changed: true,imagen:this.imagen}),
        error => this.showToast(3000, error)
    );
  }

  cancel() {
    console.log("no cierra");
    this.modalCtrl.dismiss({changed: false});
  }

  async showToast(time: number, message: string)  {
    this.toast = await this.toastController.create({
      message: message,
      duration: time
    });
      return await this.toast.present();
  }

}
