import { Component, OnInit, Input } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { UserService } from 'src/app/users/services/user.service';

@Component({
  selector: 'app-change-profile',
  templateUrl: './change-profile.component.html',
  styleUrls: ['./change-profile.component.scss']
})
export class ChangeProfileComponent implements OnInit {
  @Input() email: string;
  @Input() name: string;
  toast;
  
  constructor(public modalCtrl: ModalController,
    public userServ: UserService, public toastController: ToastController) { }

  ngOnInit() {
  }

  changeProfile(){
    this.userServ.changeUserProfile(this.name,this.email).subscribe(
      ok => this.modalCtrl.dismiss({changed: true , email:this.email,name:this.name}),
      error => this.showToast(3000, error)
  );
  }

  async showToast(time: number, message: string)  {
    this.toast = await this.toastController.create({
      message: message,
      duration: time
    });
      return await this.toast.present();
  }


  cancel() {
    console.log("no cierra");
    this.modalCtrl.dismiss({changed: false});
  }



}
