import { Component, Input } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { UserService } from 'src/app/users/services/user.service';

@Component({
  selector: 'app-change-pass',
  templateUrl: './change-pass.component.html',
  styleUrls: ['./change-pass.component.scss']
})
export class ChangePassComponent  {
  @Input() name: string;
  password = '';
  password2 = '';
  toast;
  constructor(public modalCtrl: ModalController,
  public userServ: UserService, public toastController: ToastController) {}

  changePass() {
      if (this.password !== this.password2) {
        this.showToast(3000, 'Passwords do not match!');
        return;
      }
      this.userServ.changePasswordProfile(this.password).subscribe(
          ok => this.modalCtrl.dismiss({changed: true}),
          error => this.showToast(3000, error)
      );
  }

  cancel() {
    console.log("no cierra");
    this.modalCtrl.dismiss({changed: false});
  }



  async showToast(time: number, message: string)  {
    this.toast = await this.toastController.create({
      message: message,
      duration: time
    });
      return await this.toast.present();
  }

}
