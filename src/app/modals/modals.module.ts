import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePassComponent } from './change-pass/change-pass.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { ChangeProfileComponent } from './change-profile/change-profile.component';
import { ChangeAvatarComponent } from './change-avatar/change-avatar.component';


@NgModule({
  declarations: [ChangePassComponent, ChangeProfileComponent, ChangeAvatarComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ]
})
export class ModalsModule { }
