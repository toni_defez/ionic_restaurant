import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogoutActivateGuard } from './guards/logout-activate.guard';
import { LoginActivateGuard } from './guards/login-activate.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full'
  },
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule',
   canActivate: [LogoutActivateGuard] },
  { path: 'restaurants', loadChildren: './restaurants/restaurants.module#RestaurantsModule' 
  ,canActivate: [LoginActivateGuard]},
  { path: 'users', loadChildren: './users/users.module#UsersModule' 
  ,canActivate: [LoginActivateGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
