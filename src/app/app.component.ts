import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './auth/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  isLogged = true;
  public appPages = [
    {
      title: 'Restaurants',
      url: '/restaurants/',
      icon: 'list'
    },
    {
      title: 'Add Restaurant',
      url: '/restaurants/add',
      icon: 'add'
    },
    {
      title:'My restaurants',
      url:'/restaurants/mine',
      icon:'restaurant'
    },
    {
      title: 'Profile',
      url: '/users/me',
      icon: 'person'
    }
  ];


  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private nav: NavController,
  ) {
    this.authService.loginChange$.subscribe( res => {
      this.isLogged = !res;
    });
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  async logout() {
    await this.authService.logout();
    this.nav.navigateRoot(['/auth/login']);
  }
}
