import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, Platform } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  email = '';
  password = '';
  lat: number;
  lng: number;
  geoSubscription: Subscription = null;
  isSubscribed = false;
  response = null;
  accessTokenFacebook;

  constructor(
    private geolocation: Geolocation,
    private authService: AuthService,
    private router: Router,
    private alertCtrl: AlertController,
    private platform: Platform,
    public gplus: GooglePlus,
    public fb: Facebook
  ) {}

  async ngOnInit() {
    this.startGeo();
    /** 
    try {
        this.response = await this.gplus.trySilentLogin({
          webClientId: '453175677514-70vtn7299ssm95pl7pmbsmta4gbvlm54.apps.googleusercontent.com'
          });
        if (this.response != null) {
          this.LoginGoogleSilent();
        }
      } catch (e) {
        console.error(e);
      }
      **/
  }

  private LoginGoogleSilent() {
    this.authService.loginGoogle(this.lat, this.lng, this.response.accessToken)
    .subscribe(() => this.router.navigate(['/restaurants']), async (error) => {
      (await this.alertCtrl.create({
        header: 'Login error Google',
        message: 'Incorrect email and/or password',
        buttons: ['Ok']
      })).present();
    });
  }

  login() {
    if (this.isSubscribed) {
      this.authService.login(this.email, this.password, this.lat, this.lng).subscribe(
        () => this.router.navigate(['/restaurants']),
        async error => {
          (await this.alertCtrl.create({
            header: 'Login error',
            message: 'Incorrect email and/or password',
            buttons: ['Ok']
          })).present();
        }
      );
    }
  }

  async loginGoogle() {
    try {
      console.log('entradndo');
      this.response =
       await this.gplus.login({'webClientId':
       '453175677514-70vtn7299ssm95pl7pmbsmta4gbvlm54.apps.googleusercontent.com'});
       if (this.response != null) {
         this.LoginGoogleSilent();
       }
    }catch(e){
      console.error(e);
    }
  }

  async loginFacebook(){
    const resp = await this.fb.login(['public_profile', 'email']);
    if (resp.status === 'connected') {
        this.accessTokenFacebook = resp.authResponse.accessToken;
        this.authService.loginFacebook(this.lat,this.lng,this.accessTokenFacebook).subscribe(
          () =>{
            this.response = this.accessTokenFacebook; 
            this.router.navigate(['/restaurants'])
          },
          async error => {
            (await this.alertCtrl.create({
              header: 'Login error',
              message: 'Incorrect email and/or password',
              buttons: ['Ok']
            })).present();
          }
        );
    }
  }

  startGeo() {
    this.geoSubscription = this.geolocation.watchPosition()
      .pipe(filter((data: any) => data.code === undefined))
      .subscribe(data => {
        this.lat = data.coords.latitude;
        this.lng = data.coords.longitude;
        console.log('lat'+ this.lat);
        console.log('lng'+ this.lng);
      });
      this.isSubscribed = true;
  }

}
