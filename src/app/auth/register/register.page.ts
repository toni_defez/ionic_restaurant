import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { filter, last } from 'rxjs/operators';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AuthService } from '../services/auth.service';
import {
  NavController,
  ToastController,
  LoadingController
} from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss']
})
export class RegisterPage implements OnInit {
  user = {
    name: '',
    password: '',
    email: '',
    avatar: '',
    lat: 0,
    lng: 0
  };
  password2 = '';
  lat: number;
  lng: number;
  geoSubscription: Subscription = null;
  isSubscribed = false;

  constructor(
    private authService: AuthService,
    private geolocation: Geolocation,
    private nav: NavController,
    private camera: Camera,
    private toast: ToastController,
    public loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.startGeo();
  }

  async register() {
    const loading = await this.loadingController.create({
      message: 'loading...'
    });
    this.user.lat = this.lat;
    this.user.lng = this.lng;

    await loading.present().then(() => {
      this.authService.register(this.user).subscribe(
        async () => {
          loading.dismiss();
          (await this.toast.create({
            duration: 3000,
            position: 'bottom',
            message: 'User registered!'
          })).present();
          this.nav.navigateRoot(['/auth/login']);
        },
        async err => {
          (await this.toast.create({
            duration: 3000,
            position: 'bottom',
            message: 'Email is already present in the database'
          })).present();
          loading.dismiss();
        }
      );
    });
  }

  takePhoto() {
    const options: CameraOptions = {
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    this.getPicture(options);
  }

  pickFromGallery() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    this.getPicture(options);
  }

  private getPicture(options: CameraOptions) {
    this.camera
      .getPicture(options)
      .then(imageData => {
        // If it's base64:
        this.user.avatar = 'data:image/jpeg;base64,' + imageData;
      })
      .catch(err => {
        // Handle error
      });
  }
  //  private async getPicture(options: CameraOptions) {}

  startGeo() {
    this.geoSubscription = this.geolocation
      .watchPosition()
      .pipe(filter((data: any) => data.code === undefined))
      .subscribe(data => {
        this.lat = data.coords.latitude;
        this.lng = data.coords.longitude;
        console.log('lat' + this.lat);
        console.log('lng' + this.lng);
      });

    this.isSubscribed = true;
  }
}
