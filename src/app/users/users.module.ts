import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { ModalsModule } from '../modals/modals.module';
import { ChangePassComponent } from '../modals/change-pass/change-pass.component';
import { ChangeProfileComponent } from '../modals/change-profile/change-profile.component';
import { ChangeAvatarComponent } from '../modals/change-avatar/change-avatar.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    UsersRoutingModule,
    ModalsModule
  ],
  entryComponents: [
    ChangePassComponent,
    ChangeProfileComponent,
    ChangeAvatarComponent
  ]
})
export class UsersModule { }
