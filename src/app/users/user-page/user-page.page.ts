import { Component, OnInit } from '@angular/core';
import { User } from '../interfaces/user.interface';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { ChangePassComponent } from 'src/app/modals/change-pass/change-pass.component';
import { ChangeProfileComponent } from 'src/app/modals/change-profile/change-profile.component';
import { ChangeAvatarComponent } from 'src/app/modals/change-avatar/change-avatar.component';



@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.page.html',
  styleUrls: ['./user-page.page.scss'],
})
export class UserPagePage implements OnInit {


  user: User;
  lat: number = 38.4039418;
  lng: number = -0.5288701;
  zoom: number = 12;
  toast: HTMLIonToastElement;

  ngOnInit(): void {
  }
  constructor(
    private router: Router,
    public modalCtrl: ModalController,
    private route: ActivatedRoute,
    public toastController: ToastController
  ) {
    this.user = this.route.snapshot.data['user'];
    this.lat = this.user.lat;
    this.lng = this.user.lng;
   }

   async editPassword() {
    const modal = await this.modalCtrl.create({
    component: ChangePassComponent,
    componentProps: { name: this.user.name }
    });
    await modal.present();
    const result = await modal.onDidDismiss();
    if (result.data.changed) {
        this.showToast(3000, 'Password updated successfully!');
    }
  }

  async editAvatar(){
    const modal = await this.modalCtrl.create({
      component: ChangeAvatarComponent,
      componentProps: { name: this.user.name }
      });
      await modal.present();
      const result = await modal.onDidDismiss();
      if (result.data.changed) {
          this.user.avatar = result.data.imagen;
          this.showToast(3000, 'Avatar updated successfully!');
      }
  }

  async editProfile() {
    const modal = await this.modalCtrl.create({
    component: ChangeProfileComponent,
    componentProps: { name: this.user.name , email:this.user.email }
    });
    await modal.present();
    const result = await modal.onDidDismiss();
    if (result.data.changed) {
        this.user.name= result.data.name;
        this.user.email = result.data.email;
        this.showToast(3000, 'Profile updated successfully!');
    }
  }

  async showToast(time: number, message: string)  {
    this.toast = await this.toastController.create({
      message: message,
      duration: time
    });
      return await this.toast.present();
  }



}
