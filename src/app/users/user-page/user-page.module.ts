import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { UserPagePage } from './user-page.page';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';


const routes: Routes = [
  {
    path: '',
    component: UserPagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    NgxMapboxGLModule
  ],
  declarations: [UserPagePage]
})
export class UserPagePageModule {}
