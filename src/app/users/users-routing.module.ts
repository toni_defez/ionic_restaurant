import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserGuard } from './guards/user.guard';

const routes: Routes = [
  { path: 'me', loadChildren: './user-page/user-page.module#UserPagePageModule' ,
  pathMatch: 'full'
  , resolve:{
    user: UserGuard
  }
},
  { path: 'edit', loadChildren: './user-form/user-form.module#UserFormPageModule' ,
  pathMatch: 'full',
  resolve:{
    user:UserGuard
  }},
  {
    path: ':id',
    loadChildren: './user-page/user-page.module#UserPagePageModule',
    pathMatch:'full',
    resolve:{
      user:UserGuard
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
