import { Component, OnInit, NgZone } from '@angular/core';
import { CommentRestaurant } from '../../Interfaces/comment.interface';
import { AlertController, PopoverController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { RestaurantService } from '../../services/restaurant.service';
import { CommentPopOverComponent } from 'src/app/popover/comment-pop-over/comment-pop-over.component';

@Component({
  selector: 'app-restaurant-comments',
  templateUrl: './restaurant-comments.page.html',
  styleUrls: ['./restaurant-comments.page.scss'],
})
export class RestaurantCommentsPage implements OnInit {
  idProd: number;
  comments: CommentRestaurant[];

  constructor(
    public popoverCtrl: PopoverController,
    private route: ActivatedRoute,
    private restaurantService: RestaurantService,
    private ngZone: NgZone
  ) {}

  ngOnInit() {
    this.idProd = +this.route.snapshot.parent.parent.params.id;
    this.restaurantService
      .getCommentRestaurant(this.idProd)
      .subscribe(comments => (this.comments = comments));
  }


  async addComment() {
    const popover = await this.popoverCtrl.create({
      component: CommentPopOverComponent,
      componentProps: {
      title: 'Your comment' // Input parameters
      }
      });
      await popover.present();
      const result = (await popover.onDidDismiss()).data;

      if ( result !=undefined    && result.ok === true) {
        this.restaurantService
          .addCommentToRestaurant(this.idProd,+result.stars,result.text)
          .subscribe(comment => this.ngZone.run(() => this.comments.push(comment)),
          error=>console.log(error));
      }
    /*
    const alert = await this.alertCtrl.create({
      header: 'New commment',
      inputs: [
        {
          name: 'comment',
          type: 'text',
          placeholder: 'Enter your comment'
        },
        {
          name: 'diooooos',
          min: 1,
          max: 5,
          placeholder: 'dio',
          type: 'range' // here the error
        }
      ],
      
      buttons: [
      
        {
          text: 'Add',
          role: 'ok'
        },
        {
          role: 'cancel',
          text: 'Cancel'
        }
      ]
    });
      
    await alert.present();
    const result = await alert.onDidDismiss();
      */
  
  }

}
