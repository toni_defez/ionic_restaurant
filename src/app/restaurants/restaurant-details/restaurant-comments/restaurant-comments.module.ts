import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RestaurantCommentsPage } from './restaurant-comments.page';
import { PopoverModule } from 'src/app/popover/popover.module';
import { CommentPopOverComponent } from 'src/app/popover/comment-pop-over/comment-pop-over.component';

const routes: Routes = [
  {
    path: '',
    component: RestaurantCommentsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PopoverModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [
    CommentPopOverComponent
  ],
  declarations: [RestaurantCommentsPage]
})
export class RestaurantCommentsPageModule {}
