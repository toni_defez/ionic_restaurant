import { Component, OnInit, NgZone, AfterViewInit, ViewChild } from '@angular/core';
import { Restaurant } from '../../Interfaces/restaurant.interface';
import { Events, AlertController, NavController, LoadingController } from '@ionic/angular';
import { RestaurantService } from '../../services/restaurant.service';
import { MapComponent } from 'ngx-mapbox-gl';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

@Component({
  selector: 'app-restaurant-map',
  templateUrl: './restaurant-map.page.html',
  styleUrls: ['./restaurant-map.page.scss'],
})
export class RestaurantMapPage  implements AfterViewInit {
  rest:Restaurant;
  lat: number = 38.4039418;
  lng: number = -0.5288701;
  zoom: number = 12;
  
  @ViewChild(MapComponent) mapComp: MapComponent;


  constructor(
    private events: Events,
    private launchNavigator: LaunchNavigator,
    public loadingController: LoadingController,
  ) { }

  async ngAfterViewInit() {
    this.mapComp.load.subscribe(
      () => {
        console.log(this.mapComp);
        console.log(this.mapComp.mapInstance);
        this.mapComp.mapInstance.resize(); // Necessary for full height
        loading.dismiss();
      },
      err => loading.dismiss()
    );
    this.events.subscribe('restaurant', product => {
      this.rest = product;
      console.log(this.rest);
    });
    const loading = await this.loadingController.create({
      message: 'loading restaurant'
    });
  }
  async navigate() {
    const options: LaunchNavigatorOptions = {};
    await this.launchNavigator.navigate([this.lat, this.lng], options);
  }

}
