import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RestaurantDetailsPage } from './restaurant-details.page';
import { CommentPopOverComponent } from 'src/app/popover/comment-pop-over/comment-pop-over.component';
import { PopoverModule } from 'src/app/popover/popover.module';

const routes: Routes = [
  {
    path: '',
    component: RestaurantDetailsPage,
    children: [
      { path: 'info', loadChildren: './restaurant-info/restaurant-info.module#RestaurantInfoPageModule' },
      { path: 'map', loadChildren: './restaurant-map/restaurant-map.module#RestaurantMapPageModule' },
      { path: 'comments', loadChildren: './restaurant-comments/restaurant-comments.module#RestaurantCommentsPageModule' },
      { path: '', pathMatch: 'full', redirectTo: 'info' }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PopoverModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RestaurantDetailsPage]
})
export class RestaurantDetailsPageModule {}
