import { Component, OnInit, NgZone } from '@angular/core';
import { RestaurantService } from '../../services/restaurant.service';
import { Events, AlertController, NavController } from '@ionic/angular';
import { Restaurant } from '../../Interfaces/restaurant.interface';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-restaurant-info',
  templateUrl: './restaurant-info.page.html',
  styleUrls: ['./restaurant-info.page.scss'],
})
export class RestaurantInfoPage implements OnInit {
  rest:Restaurant;
  zoom: number = 12;
 
  constructor(
    private events: Events,
    private alertCrl: AlertController,
    private resttService: RestaurantService,
    private nav: NavController,
    private ngZone: NgZone,
    private router: Router
  ) {}

  ngOnInit() {
    this.events.subscribe('restaurant', product => {
      this.rest = product;
    });
  }

  showRestaurantsUser()  {
    this.router.navigate(['/restaurants/user',this.rest.creator.id]);
  }
  async delete() {
    const alert = await this.alertCrl.create({
      header: 'Delete Restaurant',
      message: 'Are you sure you want to delete this restaurant?',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.resttService
              .deleteRestaurant(this.rest.id)
              .subscribe(() => this.nav.navigateBack(['/restaurants']));
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    alert.present();
  }


}
