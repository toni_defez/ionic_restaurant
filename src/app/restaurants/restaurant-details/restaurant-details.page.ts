import { Component, OnInit } from '@angular/core';
import { RestaurantService } from '../services/restaurant.service';
import { ActivatedRoute } from '@angular/router';
import { Events } from '@ionic/angular';
import { Restaurant } from '../Interfaces/restaurant.interface';

@Component({
  selector: 'app-restaurant-details',
  templateUrl: './restaurant-details.page.html',
  styleUrls: ['./restaurant-details.page.scss'],
})
export class RestaurantDetailsPage implements OnInit {

  rest: Restaurant;
  constructor(private restService: RestaurantService, private route: ActivatedRoute, private events: Events ){ }

  ngOnInit() {
    this.restService.getRestaurant(this.route.snapshot.params.id).subscribe(
      rest => {
        this.rest = rest;
        this.events.publish('restaurant', this.rest);
      }
    );
  }

  tabsChange(events) {
    /*
    if (events.tab === 'info') {
      this.events.publish('rest', this.rest.name);
    }
    */
    this.events.publish('restaurant', this.rest);
  }

}
