import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { patchComponentDefWithScope } from '@angular/core/src/render3/jit/module';
import { RestaurantResolver } from './guards/restaurant.guard';
import { Show } from 'src/enums/show.enum';

const routes: Routes = [
  {
    path: 'add',
    loadChildren:
      './restaurant-form/restaurant-form.module#RestaurantFormPageModule',
    pathMatch: 'full'
  },
  {
    path: 'edit/:id',
    loadChildren:
      './restaurant-form/restaurant-form.module#RestaurantFormPageModule',
    pathMatch: 'full',
    resolve: {
      restaurant: RestaurantResolver
    }
  },
  {
    path: 'details/:id',
    loadChildren:
      './restaurant-details/restaurant-details.module#RestaurantDetailsPageModule'
  },
  {
    path: 'restaurant-info',
    loadChildren:
      './restaurant-details/restaurant-info/restaurant-info.module#RestaurantInfoPageModule'
  },
  {
    path: 'restaurant-comments',
    loadChildren:
      './restaurant-details/restaurant-comments/restaurant-comments.module#RestaurantCommentsPageModule'
  },
  {
    path: 'restaurant-map',
    loadChildren:
      './restaurant-details/restaurant-map/restaurant-map.module#RestaurantMapPageModule'
  },
  {
    path: '',
    loadChildren:
      './restaurant-list/restaurant-list.module#RestaurantListPageModule',
    pathMatch: 'full',
    data: { show: Show.ALL }
  },
  {
    path: 'user/:id',
    loadChildren:
      './restaurant-list/restaurant-list.module#RestaurantListPageModule',
    pathMatch: 'full',
    data: { show: Show.ASSIST }
  },
  {
    path: 'mine',
    loadChildren:
      './restaurant-list/restaurant-list.module#RestaurantListPageModule',
    pathMatch: 'full',
    data: { show: Show.MINE }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestaurantsRoutingModule {}
