import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Restaurant } from '../Interfaces/restaurant.interface';
import { CommentRestaurant } from '../Interfaces/comment.interface';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  readonly BASE_URL = 'restaurants';
  constructor(private http: HttpClient) {}

  //todos los restaurantes
  getRestaurants(): Observable<Restaurant[]> {
    return this.http.get<{ restaurants: Restaurant[] }>(this.BASE_URL).pipe(
      map(resp =>
        resp.restaurants.map(r => {
          r.image = `${environment.baseUrl}/${r.image}`;
          r.distance = Math.round(r.distance * 100) / 100;
          return r;
        })
      )
    );
  }

  //Mis restaurantes
  getRestaurantMine():Observable<Restaurant[]>{
    return this.http.get<{ restaurants: Restaurant[] }>('restaurants/mine').pipe(
      map(resp =>
        resp.restaurants.map(r => {
          r.image = `${environment.baseUrl}/${r.image}`;
          r.distance = Math.round(r.distance * 100) / 100;
          return r;
        })
      )
    );
  }

  getRestaurant(id: number): Observable<Restaurant> {
    return this.http.get<{restaurant: Restaurant}>(`${this.BASE_URL}/${id}`)
      .pipe(
        map(resp => {
          console.log(resp);
          const p = resp.restaurant;
          console.log(p);
          
          p.image = environment.baseUrl + '/' + p.image;
          p.creator.avatar = environment.baseUrl + '/' + p.creator.avatar;
          
          return p;
        })
      );
  }

  //restaurantes de otros usuarios
  getRestaurantsByUser(id:number){
    return this.http.get<{ restaurants: Restaurant[] }>(`restaurants/user/${id}`).pipe(
      map(resp =>
        resp.restaurants.map(r => {
          r.image = `${environment.baseUrl}/${r.image}`;
          r.distance = Math.round(r.distance * 100) / 100;
          return r;
        })
      )
    );
  }

  addRestaurant(rest: Restaurant): Observable<Restaurant> {
    console.log(rest);
    return this.http.post<{restaurant: Restaurant}>(this.BASE_URL, {
      "name":rest.name,
      "description":rest.description,
      "daysOpen":rest.daysOpen,
      "cuisine":rest.cuisine,
      "phone":rest.phone,
      "address": rest.address,
      "lat": +rest.lat,
      "lng": +rest.lng,
      "image": rest.image
    })
      .pipe(map(resp => {
        console.log("RESPUETA");
        console.log(resp);
        const r = resp.restaurant;
        r.image = `${environment.baseUrl}/${r.image}`;
        return r;
      }));
  }

  getCommentRestaurant(idRestaurant:number){
    return this.http.get<{comments:CommentRestaurant[]}>(`restaurants/${idRestaurant}/comments`).pipe(
        map(resp=>{
          const comments = resp.comments;
          comments.map(c=>{
            c.user.avatar = `${environment.baseUrl}/${c.user.avatar}`;
          })
          return comments;
        })
    )
  }

  updateRestaurant(rest:Restaurant):Observable<Restaurant>{
    console.log(rest);
    return this.http.put<{restaurant:Restaurant}>(`restaurants/${rest.id}`,{
      "name":rest.name,
      "description":rest.description,
      "daysOpen":rest.daysOpen,
      "cuisine":rest.cuisine,
      "phone":rest.phone,
      "address": rest.address,
      "lat": +rest.lat,
      "lng": +rest.lng,
      "image": rest.image
    }).pipe(
      map(resp=>{
        const r = resp.restaurant;
        r.image = `${environment.baseUrl}/${r.image}`;
        return r;
      })
    );
  }

  addCommentToRestaurant(idRestaurant:number,stars:number,
    text:string){
      console.log("estresllas"+stars);
      return this.http.post<{comment:CommentRestaurant}>(`restaurants/${idRestaurant}/comments`,
      {
        "stars":stars,
        "text":text
      }).pipe(
        map(resp=>{
          const r = resp.comment;
          r.user.avatar = `${environment.baseUrl}/${r.user.avatar}`;
          return r;
        })
      )
    }

  deleteRestaurant(idRest): Observable<void> {
    return this.http.delete(`${this.BASE_URL}/${idRest}`).pipe(map(() => null));
  }
}
