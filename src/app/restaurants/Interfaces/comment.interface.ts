import { User } from 'src/app/users/interfaces/user.interface';

export interface CommentRestaurant {
    id: number;
    stars: number;
    text: string;
    date: Date;
    user: User;
}