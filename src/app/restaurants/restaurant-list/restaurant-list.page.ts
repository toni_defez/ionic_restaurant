import { Component, OnInit } from "@angular/core";
import { Restaurant } from "../Interfaces/restaurant.interface";
import { RestaurantService } from "../services/restaurant.service";
import { ActionSheetController } from "@ionic/angular";
import { Router, ActivatedRoute } from "@angular/router";
import { Show } from "src/enums/show.enum";

@Component({
  selector: "app-restaurant-list",
  templateUrl: "./restaurant-list.page.html",
  styleUrls: ["./restaurant-list.page.scss"]
})
export class RestaurantListPage implements OnInit {
  restaurants: Restaurant[] = [];
  finished = false;
  readonly days = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];
  weekDay: number;
  auxRating: number;
  distance: number;
  userOther: number;
  filteredItems=[];

  constructor(
    private restaurantService: RestaurantService,
    private route: ActivatedRoute,
    private actionSheetCtrl: ActionSheetController,
    private router: Router
  ) {}

  ngOnInit() {
    this.loadMoreItems(null);
    this.weekDay = new Date().getDay();
  }
  getDaysString(rest) {
    return rest.daysOpen.map(d => this.days[d]).join(", ");
  }

  refresh(event) {
    this.loadMoreItems(event);
    this.filteredItems = this.restaurants;
    /** 
    this.restaurantService.getRestaurants().subscribe(
      prods =>{ 
        this.restaurants = prods
        console.log(this.restaurants);
        event.target.complete(); 
      },
      (error=>{
        this.finished = true;
      })
    );
    **/
  }

  loadMoreItems(event) {
    let ObservableRestaurant=null;
    switch (this.route.snapshot.data.show) {
      case Show.ALL:
        ObservableRestaurant= this.restaurantService.getRestaurants();
        break;
      case Show.MINE:
        console.log("Mios");
        ObservableRestaurant= this.restaurantService.getRestaurantMine();
        break;
      case Show.ASSIST:
        let id = this.route.snapshot.paramMap.get("id");
        ObservableRestaurant= this.restaurantService.getRestaurantsByUser(+id);
        break;
    }
    ObservableRestaurant.subscribe(
      prods => {
        this.restaurants = prods;
        console.log(this.restaurants);
        if(event!=null){
          event.target.complete();
        }
        this.filteredItems = this.restaurants;
      },
      error => {
        this.finished = true;
      }
    );
  }

  filterItems(event) {
    let search: string = event.target.value;
      console.log(search);
    if (search && search.trim() !== '') {
      search = search.trim().toLowerCase();
      this.filteredItems = this.restaurants.filter
        (r => r.name.toLocaleLowerCase().includes(search.toLocaleLowerCase()));
    } else {
      this.filteredItems = this.restaurants;
    }
    console.log("filtered");
    console.log(this.filteredItems);
  }

  async showOptions(rest: Restaurant) {
    let  actSheet =null;
    if(rest.mine){
       actSheet = await this.actionSheetCtrl.create({
        header: rest.description,
        buttons: [
          {
            text: "Delete",
            role: "destructive",
            icon: "trash",
            handler: () => {
              this.restaurantService
                .deleteRestaurant(rest.id)
                .subscribe(() =>
                  this.restaurants.splice(this.restaurants.indexOf(rest), 1)
                );
            }
          },
          {
            text: "See details",
            icon: "eye",
            handler: () => {
              this.router.navigate(["/restaurants/details", rest.id]);
            }
          },
          {
            text: "Edit restaurant",
            icon: "eye",
            handler: () => {
              this.router.navigate(["/restaurants/edit", rest.id]);
            }
          },
          {
            text: "Cancel",
            icon: "close",
            role: "cancel"
          }
        ]
      });
    }
    else{
       actSheet = await this.actionSheetCtrl.create({
        header: rest.description,
        buttons: [
          {
            text: "See details",
            icon: "eye",
            handler: () => {
              this.router.navigate(["/restaurants/details", rest.id]);
            }
          },
          {
            text: "Cancel",
            icon: "close",
            role: "cancel"
          }
        ]
      });
    }
    actSheet.present();
  }
}
