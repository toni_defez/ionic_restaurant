import { Component, OnInit, ViewChild } from '@angular/core';

import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import {
  ToastController,
  NavController,
  LoadingController
} from '@ionic/angular';
import { Restaurant } from '../Interfaces/restaurant.interface';
import { RestaurantService } from '../services/restaurant.service';
import { Result } from 'ngx-mapbox-gl/lib/control/geocoder-control.directive';
import { ActivatedRoute } from '@angular/router';
import { MapComponent } from 'ngx-mapbox-gl';

@Component({
  selector: 'app-restaurant-form',
  templateUrl: './restaurant-form.page.html',
  styleUrls: ['./restaurant-form.page.scss']
})
export class RestaurantFormPage implements OnInit {
  readonly days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  newRest: Restaurant = null;
  daysOpen: boolean[] = [];
  zoom = 17;
  lat = 1;
  lng = 2;
  edit = false;
  @ViewChild(MapComponent) mapComp: MapComponent;

  constructor(
    private restaurantService: RestaurantService,
    private toastCtrl: ToastController,
    private nav: NavController,
    private camera: Camera,
    public loadingController: LoadingController,
    private route: ActivatedRoute
  ) {}

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'loading restaurant'
    });
    await loading.present().then(() => {
      this.newRest = this.route.snapshot.data.restaurant;
      this.edit = true;
      this.daysOpen = new Array(7).fill(false);
      for (let i = 0; i < this.newRest.daysOpen.length; i++) {
        this.daysOpen[this.newRest.daysOpen[i]] = true;
      }
      this.mapComp.load.subscribe(
        () => {
          console.log(this.mapComp);
          console.log(this.mapComp.mapInstance);
          this.mapComp.mapInstance.resize(); // Necessary for full height
          loading.dismiss();
        },
        err => loading.dismiss()
      );
    });
  }
  async ngOnInit() {
    const id = +this.route.snapshot.params.id;
    if (!isNaN(id)) {
      this.edit = true;
      await this.presentLoading();
    } else {
      this.resetForm();
    }
  }

  resetForm() {
    this.daysOpen = new Array(7).fill(true);
    this.newRest = {
      name: '',
      cuisine: '',
      daysOpen: [],
      description: '',
      image: null,
      phone: '',
      address: 'None',
      lat: 38.4039418,
      lng: -0.5288701,
      creator: null
    };
  }

  async addRestaurant() {
    let observableService;
    let text;
    this.newRest.daysOpen = this.daysOpen.reduce(
      (days, isSelected, i) => (isSelected ? [...days, i] : days),
      []
    );
    console.log('Nos vamos a la mierda');
    console.log(this.newRest);
    if (!this.edit) {
      observableService = this.restaurantService.addRestaurant(this.newRest);
      text = 'Creating new restaurant';
    } else {
      observableService = this.restaurantService.updateRestaurant(this.newRest);
      text = 'Updating restaurant';
    }

    const loading = await this.loadingController.create({
      message: 'loading...'
    });
    await loading.present().then(() => {
      observableService.subscribe(
        async prod => {
          loading.dismiss();
          (await this.toastCtrl.create({
            position: 'bottom',
            duration: 3000,
            message: text,
            color: 'success'
          })).present();
          this.nav.navigateRoot(['/restaurants']);
        },
        async error => {
          console.log(error);
          loading.dismiss();
          (await this.toastCtrl.create({
            position: 'bottom',
            duration: 3000,
            message: 'Something wrong passed'
          })).present();
        }
      );
    });
  }

  changePosition(result: Result) {
    this.newRest.lat = +result.geometry.coordinates[1];
    this.newRest.lng = +result.geometry.coordinates[0];
    console.log('New address: ' + result.place_name);
  }

  takePhoto() {
    const options: CameraOptions = {
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };
    this.getPicture(options);
  }

  pickFromGallery() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    this.getPicture(options);
  }

  private getPicture(options: CameraOptions) {
    this.camera
      .getPicture(options)
      .then(imageData => {
        // If it's base64:
        this.newRest.image = 'data:image/jpeg;base64,' + imageData;
      })
      .catch(err => {
        // Handle error
        console.log('Error al hacer foto');
      });
  }
}
