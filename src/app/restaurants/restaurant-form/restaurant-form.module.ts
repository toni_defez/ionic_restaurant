import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RestaurantFormPage } from './restaurant-form.page';
import { ValidatorModule } from 'src/app/validator/validator.module';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

const routes: Routes = [
  {
    path: '',
    component: RestaurantFormPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ValidatorModule,
     NgxMapboxGLModule
  ],
  declarations: [RestaurantFormPage]
})
export class RestaurantFormPageModule {}
