import { TestBed, async, inject } from '@angular/core/testing';

import { RestaurantResolver } from './restaurant.guard';

describe('RestaurantGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RestaurantResolver]
    });
  });

  it('should ...', inject([RestaurantResolver], (guard: RestaurantResolver) => {
    expect(guard).toBeTruthy();
  }));
});
