import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Resolve, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Restaurant } from '../Interfaces/restaurant.interface';
import { RestaurantService } from '../services/restaurant.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestaurantResolver implements Resolve<Restaurant> {
  constructor(private restService: RestaurantService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Restaurant> {
    return this.restService.getRestaurant(route.params['id']).pipe(
      catchError(error => {
        this.router.navigate(['/restaurants']);
        return of(null);
      })
    );
  }
}