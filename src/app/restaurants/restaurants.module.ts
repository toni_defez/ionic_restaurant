import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RestaurantsRoutingModule } from './restaurants-routing.module';
import { ValidatorModule } from '../validator/validator.module';
import { PopoverModule } from '../popover/popover.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RestaurantsRoutingModule,
    PopoverModule,
  
  ]
})
export class RestaurantsModule { }
