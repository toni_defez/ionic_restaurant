import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentPopOverComponent } from './comment-pop-over/comment-pop-over.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CommentPopOverComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ]
})
export class PopoverModule { }
