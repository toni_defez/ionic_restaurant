import { Component, OnInit, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-comment-pop-over',
  templateUrl: './comment-pop-over.component.html',
  styleUrls: ['./comment-pop-over.component.scss']
})
export class CommentPopOverComponent  {
  text: string;
  rating=0;
  constructor(public popoverCtrl: PopoverController){}

  send() {
    console.log(this.rating);
    this.popoverCtrl.dismiss({'ok':true,'stars':this.rating,'text':this.text});
  }

}
