import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentPopOverComponent } from './comment-pop-over.component';

describe('CommentPopOverComponent', () => {
  let component: CommentPopOverComponent;
  let fixture: ComponentFixture<CommentPopOverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentPopOverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentPopOverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
